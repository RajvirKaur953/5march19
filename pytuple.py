# ls=['red','green','blue','red','red']     #(list is mutable i.e can be changed)
# ls[1]='yellow'
# print(ls)

# # tuple doesn't support item assignment
# # t=tuple(ls)
# # t[1]='green'
# # print(t)

# c=ls.count('red')
# print(c)

#creating a tuple(immutable)

tp=()
# tp[0]='hello'
# print(tp)
tp=(10,20,30,40,50,60,89,00,863,1,3,6,20)
print(tp[0])
print(tp[-1])
print(tp[1:])
print(tp[:1])
print(tp[:-1])
print(tp[::2])
print(tp[::3])