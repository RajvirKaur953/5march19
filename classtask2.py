#TOLOWER CASE
names=['aFGH','ASD','Aman','DFGHJ','hello','pyTHOn','SDFG']
c=[i.lower() for i in names]
print(c)

# lw=[]                                                 #without list comprehension
# for i in names:
#     lw.append(i.lower())
# print(lw)

#TO UPPERCASE 
names=['aFGH','ASD','Aman','DFGHJ','hello','pyTHOn','SDFG']
lc=[i.upper() for i in names]
print(lc)

#TO CAPITALIZE FIRST CHARACTER
names=['aFGH','ASD','Aman','DFGHJ','hello','pyTHOn','SDFG']
a=[i.capitalize() for i in names]
print(a)