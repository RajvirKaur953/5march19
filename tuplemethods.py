# show tuple
a=10,30,69,100
b=10,30,69,100,899
# print(type(a))

# length of tuple
length=len(a)
# print(length)

# max and min value
m=max(a)
m=min(a)
v=max(b)
print(m)
print(v)

# concatenation
conc=a+b
print(conc)

# repetition
rp=a*3
print(rp)

# membership
chk=30 in a
print(chk)
chk2=90 in a
print(chk2)
chk1=30 not in a
print(chk1)
chk3=90 not in a
print(chk3)

#index
ind=a.index(100)
print(ind)
