ls=['red','green','blue']
st=int('103')
n=10

#convert to string
convert_to_string=str(n)
#print(type(n))
print(type(convert_to_string))

#convert to boolean
b=bool(n)
print(type(b))

#convert to float
f=float(n)
print(type(f))
print(f)

#convert to list
# n=10
# print(list[0])            number can't be converted into list because we can't index numbers,but string
l='String'
print(list(l))

# convert to tuple
t=tuple(l)                      #can't be converted into directory because there are no key:value pairs
print(t)
